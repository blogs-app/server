const mysql = require('mysql')

const openConnection = () => {
    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'manager',
        database: 'blogs_db',
        port: 3306
    })

    connection.connect()

    return connection
}

module.exports = {
    openConnection
}