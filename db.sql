CREATE DATABASE blogs_db;

use blogs_db;

CREATE TABLE users (
    id INTEGER PRIMARY KEY auto_increment,
    firstName VARCHAR(50),
    lastName VARCHAR(50),
    password VARCHAR(600),
    email VARCHAR(50),
    birthDate DATE,
    addressLine1 VARCHAR(50),
    addressLine2 VARCHAR(50),
    city VARCHAR(50),
    state VARCHAR(50),
    country VARCHAR(50),
    postalCode INTEGER,
    phone VARCHAR(50),
    gender INT(1), /* 0:female 1:male*/
    profileImage VARCHAR(50),
    isActive INT(1) DEFAULT 1,
    createdTimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE blogs (
    id INTEGER PRIMARY KEY auto_increment,
    userId INTEGER,
    title VARCHAR(100),
    details VARCHAR(1024),
    tags VARCHAR(500),
    /*0: draft, 1: private, 2: public*/
    state INT(1) DEFAULT 0,
    createdTimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    Foreign Key (userId) REFERENCES users(id)
);

CREATE TABLE blog_attachment (
    id INTEGER PRIMARY KEY auto_increment,
    type VARCHAR(10),
    url VARCHAR(100),
    blogId INTEGER,
    createdTimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    Foreign Key (blogId) REFERENCES blogs(id)
);

CREATE TABLE blog_comment (
    id INTEGER PRIMARY KEY auto_increment,
    userId INTEGER,
    blogId INTEGER,
    comment VARCHAR(1024),
    createdTimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    Foreign Key (blogId) REFERENCES blogs(id),
    Foreign Key (userId) REFERENCES users(id)
);

CREATE TABLE blog_rating (
    id INTEGER PRIMARY KEY auto_increment,
    userId INTEGER,
    blogId INTEGER,
    rating FLOAT,
    createdTimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    Foreign Key (blogId) REFERENCES blogs(id),
    Foreign Key (userId) REFERENCES users(id)
);

CREATE TABLE blog_like_status (
    id INTEGER PRIMARY KEY auto_increment,
    userId INTEGER,
    blogId INTEGER,
    type VARCHAR(1),
    createdTimeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    Foreign Key (blogId) REFERENCES blogs(id),
    Foreign Key (userId) REFERENCES users(id)
);