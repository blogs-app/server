const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/', (req,res)=>{
    const {title, details, tags, userId} = req.body
    const statement = `
        insert into blogs
            (title, details, tags, userId)
        values
            ('${title}', '${details}', '${tags}', '${userId}');
        `
    const connection = db.openConnection()
    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result))
    })
})

router.get('/search', (req,res)=>{
    const{user, tag, title} = req.query

    const conditions = []

    // check if user exists in a query string
    // /blog/search?user=1
    if(user){
        conditions.push(` userId = '${user}' `)
    }

    // check if title exists in a query string
    // /blog/search?title=react
    if(title){
        conditions.push(` title like '%${title}%' `)
    }

    // check if tag exists in a query string
    // /blog/search?tag=js
    if(tag){
        conditions.push(` tags like '%${tag}%' `)
    }

    let where = ''

    if(conditions.length>0){
        where = `and ${conditions.join(' and ')}`
    }

    const statement = 
        `
        select 
            blogs.id, 
            blogs.title, 
            blogs.createdTimeStamp,
            blogs.details,
            blogs.tags, 
            users.firstName, 
            users.lastName
        from 
            blogs, users 
        where
            blogs.userId = users.id
            ${where};`

    const connection = db.openConnection()
    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result))
    })
})

router.put('/:id', (req,res)=>{
    const {title,details,tags} = req.body
    const {id} = req.params
    const statement = `
        update blogs 
            set
                title = '${title}',
                details = '${details}',
                tags = '${tags}'
            where 
                id=${id};
        `

    const connection = db.openConnection()
    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result))
    })
})

router.delete('/:id', (req,res)=>{
    const {id} = req.params
    const statement = `delete from blogs where id = ${id};`
    const connection = db.openConnection()
    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result))
    })
})

router.patch('/:id/state/:state', (req,res)=>{
    const {id, state} = req.params

    // 0 : draft
    // 1 : private
    // 2 : public
    
    if(state<0 || state>2){
        res.send(utils.createResult('state can only be Draft[0] or Private[1] or Public[2]'))
        return
    }

    const statement = `
        update blogs 
        set
            state = ${state}
        where 
            id=${id};
        `

    const connection = db.openConnection()
    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result))
    })
})

router.patch('/:id/toggle-like', (req, res)=>{
    const{id} = req.params
    const{type, userId} = req.body

    if(type>1 || type<0){
        res.send(utils.createResult('type can only be [like(1), dislike(0)]'))
        return
    }

    const connection = db.openConnection()
    const likeStatusStatement = `
        select * from blog_like_status
        where
            userId = ${userId} and
            blogId = ${id};
        `
    connection.query(likeStatusStatement, (error, record)=>{
        if(error){
            res.send(utils.createResult(error))
        }else{
            const statement = record.length == 0
                                ? `insert into blog_like_status (blogId, userId, type) values (${id}, ${userId}, ${type});`
                                : `update blog_like_status set type = ${type} where blogId = ${id} and userId = ${userId};`
            
            connection.query(statement, (error, result)=>{
                    connection.end()
                    res.send(utils.createResult(error, result))
            })
        }
    })
})

router.get('/:id/like-count',(req, res)=>{
    const {id} = req.params
    const connection = db.openConnection()
    const statement = `select count(*) 'like-count' from blog_like_status where blogId = ${id} and type = 1;`

    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result[0]))
    })
})

module.exports = router