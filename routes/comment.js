const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/:id', (req,res)=>{
    const{id} = req.params
    const connection = db.openConnection()

    const statement = `
        select 
            comment.id, 
            comment.comment, 
            comment.createdTimeStamp, 
            users.firstName, 
            users.lastName
        from 
            blog_comment comment, users 
        where
            comment.userId = users.id and
            comment.blogId = ${id};`

    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result))
    })
})

router.post('/:id', (req,res)=>{
    const{id} = req.params
    const{userId, comment} = req.body
    const connection = db.openConnection()

    const statement = `insert into blog_comment (userId, blogId, comment) values (${userId}, ${id}, '${comment}');`

    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result))
    })
})

router.put('/:id', (req,res)=>{
    const{id} = req.params
    const{comment} = req.body

    const connection = db.openConnection()

    const statement = `
        update blog_comment 
            set comment = '${comment}'
        where 
            id = ${id};`

    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result))
    })
})

router.delete('/:id', (req,res)=>{
    const{id} = req.params
    
    const connection = db.openConnection()

    const statement = `
        delete from blog_comment where id = ${id};`

    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result))
    })
})

module.exports = router