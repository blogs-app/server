const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/:id', (req,res)=>{
    const{id} = req.params
    const connection = db.openConnection()

    const statement = `
        select rating.id, rating.rating, rating.createdTimeStamp, users.firstName, users.lastName
        from 
            blog_rating rating, users 
        where
            rating.userId = users.id and
            rating.blogId = ${id};`

    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result))
    })
})

router.get('/:id/average', (req,res)=>{
    const{id} = req.params
    const connection = db.openConnection()

    const statement = `
        select AVG(rating) avg
        from 
            blog_rating 
        where
            blogId = ${id};`

    connection.query(statement, (error,result)=>{
        connection.end()
        res.send(utils.createResult(error,result[0]))
    })
})

router.post('/:id', (req,res)=>{
    const{id} = req.params
    const{userId, rating} = req.body
    const connection = db.openConnection()

    const ratingStatusStatement = `select * from blog_rating where blogId = ${id} and userId = ${userId};`
    
    connection.query(ratingStatusStatement, (error, records)=>{
        if(error){
            connection.end()
            res.send(utils.createResult(error))
        }else{

            const statement = records.length == 0 
                            ? `insert into blog_rating 
                                    (userId, blogId, rating) 
                               values 
                                    (${userId}, ${id}, ${rating});`
                            : `update blog_rating 
                                set rating = ${rating}
                                where userId = ${userId} and blogId = ${id};`
        
            connection.query(statement, (error,result)=>{
                connection.end()
                res.send(utils.createResult(error,result))
            })
        }
    })

})

module.exports = router