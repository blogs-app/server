const express = require('express')
const db = require('../db')
const utils = require('../utils')
const crypto = require('crypto-js')

const router = express.Router()

//API to signup the user
router.post('/signup', (req,res) => {
    const {firstName, lastName, email, password} = req.body

    const connection = db.openConnection()
    
    // check if the email sent by user already exists
    const emailStatement = `select email from users where email = '${email}';`
    connection.query(emailStatement, (error,emails)=>{
        if(error){
            //if error is generated while executing a sql statement
            res.send(utils.createResult(error))
        }else{
            if(emails.length == 0){
                //if there is no user registered with the email entered
                const encryptedPassword = crypto.SHA512(password)
                const statement = `insert into users (firstName, lastName, email, password) 
                values ('${firstName}', '${lastName}', '${email}', '${encryptedPassword}');`

                connection.query(statement, (error, result)=>{
                    connection.end()
                    res.send(utils.createResult(error,result))
                })
            }else{
                //atleast one user exists
                connection.end()
                res.send(utils.createResult('duplicate email, please use another'))
            }
        }
    })
})

router.post('/signin', (req,res) => {
    const {email, password} = req.body
    const connection = db.openConnection()
    
    const encryptedPassword = crypto.SHA512(password)

    const statement = `
        select 
            id, firstName, lastName from users 
        where 
            email = '${email}' and password = '${encryptedPassword}';
        `

    connection.query(statement, (error,users)=>{
        if(error){
            res.send(utils.createResult(error))
        }else{
            if(users.length == 0){
                //there is no user with given credencials
                connection.end()
                res.send(utils.createResult('User not found'))
            }else{
                const user = users[0]
                connection.end()
                res.send(utils.createResult(null,user))
            }
        }
    })
})

router.put('/profile/:id', (req,res) => {
    const{id} = req.params
    const { 
        firstName, 
        lastName, 
        birthDate, 
        addressLine1, 
        addressLine2, 
        city, 
        state, 
        country, 
        phone, 
        gender } = req.body

        const statement = `
        update users 
        set
            firstName = '${firstName}',
            lastName = '${lastName}',
            birthDate = '${birthDate}',
            addressLine1 = '${addressLine1}',
            addressLine2 = '${addressLine2}',
            city = '${city}',
            state = '${state}',
            country = '${country}',
            phone = '${phone}',
            gender = ${gender}
        where
            id = ${id};
        `
    
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        res.send(utils.createResult(error, result))
    })
})

router.get('/profile/:id', (req,res) => {
    const {id} = req.params

    const statement = 
        `select
            id,
            firstName, 
            lastName,
            email, 
            birthDate, 
            addressLine1, 
            addressLine2, 
            city, 
            state, 
            country, 
            phone, 
            gender
        from users where id = ${id};`

    const connection = db.openConnection()
    connection.query(statement, (error, records) => {
        connection.end()
        if(records.length > 0){
            res.send(utils.createResult(error, records[0]))
        }else{
            res.send(utils.createResult("User doesnot exist"))
        }
    })
})

/*

router.delete('/:id', (req,res) => {
    const {id} = req.params
    const connection = db.openConnection()

    const userExistStatement = `select firstName, lastName from users where id = ${id};`

    connection.query(userExistStatement, (error, records) => {
        if(error){
            connection.end()
            res.send(utils.createResult(error))
        }else{
            if(records.length > 0){
                const statement = 
                    `delete from users where id = ${id};`
            
                connection.query(statement, (error, result) => {
                    connection.end()
                    res.send(utils.createResult(error, result))
                })
            }else{
                connection.end()
                res.send(utils.createResult("User doesnot exist"))
            }
        }
    })

})

*/

module.exports = router