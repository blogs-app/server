const express = require('express')
const cors = require('cors')
const routerUser = require('./routes/user')
const routerBlog = require('./routes/blog')
const routerComment = require('./routes/comment')
const routerRating = require('./routes/rating')

const app = express()

// Enable CORS (Cross Origin Resource Sharing)
// Needed for client to call the apis from different url
app.use(cors('*'))

//add the json parser to parse the json data sent through the request body
app.use(express.json())

//use the router to find all the apis related to user
app.use('/user', routerUser)

//use the router to find all the apis related to blog
app.use('/blog', routerBlog)

//use the router to find all the apis related to comment
app.use('/comment', routerComment)

//use the router to find all the apis related to rating
app.use('/rating', routerRating)

app.listen(4000, ()=>{
    console.log('Server started on port 4000')
})